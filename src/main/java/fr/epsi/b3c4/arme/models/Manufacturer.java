package fr.epsi.b3c4.arme.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Manufacturer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String name;
	private String reputation;
	private DelivreryType delivreryType;

	@OneToMany
	@JoinColumn(name="manufacturer_id")
	private Set<Weapon> weapons;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReputation() {
		return reputation;
	}

	public void setReputation(String reputation) {
		this.reputation = reputation;
	}

	public DelivreryType getDelivreryType() {
		return delivreryType;
	}

	public void setDelivreryType(DelivreryType delivreryType) {
		this.delivreryType = delivreryType;
	}

	public Set<Weapon> getWeapon() {
		return weapons;
	}

	public void setWeapon(Set<Weapon> weapons) {
		this.weapons = weapons;
	}
		
}

enum DelivreryType {
	bomber_eat,
	lolypop_factory,
	laPoste,
	chamal
}