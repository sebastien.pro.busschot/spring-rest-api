package fr.epsi.b3c4.arme.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.b3c4.arme.models.WeaponType;
import fr.epsi.b3c4.arme.repository.TypeRepository;

@Service
public class TypeService {
	 	@Autowired
	    private TypeRepository repository;

	    public WeaponType saveType(WeaponType type) {
	        return repository.save(type);
	    }

	    public List<WeaponType> saveTypes(List<WeaponType> types) {
	        return repository.saveAll(types);
	    }

	    public List<WeaponType> getTypes() {
	        return repository.findAll();
	    }

	    public WeaponType getTypeById(Long id) {
	        return repository.findById(id).orElse(null);
	    }


	    public String deleteType(Long id) {
	        try {
	            repository.deleteById(id);
	            return "remove" + id;
	        } catch (Exception e) {
	            return null;
	        }
	    }

	    public WeaponType updateType(Long id,WeaponType type) {
	        WeaponType existingType = repository.findById(id).orElse(null);
	        if(existingType == null){
	            return null;
	        }
	        existingType.setLabel(type.getLabel());
	        return repository.save(existingType);
	    }
}
