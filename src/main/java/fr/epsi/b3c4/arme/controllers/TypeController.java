package fr.epsi.b3c4.arme.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epsi.b3c4.arme.models.WeaponType;
import fr.epsi.b3c4.arme.services.TypeService;

@RestController
@RequestMapping("/api/types")
public class TypeController {
	
	@Autowired
	private TypeService service;
	
	@GetMapping()
	public List<WeaponType> type () {
		return service.getTypes();
	}
	
	@GetMapping("{id}")
	public WeaponType type (@PathVariable long id) {
		return service.getTypeById(id);
	}
	
	@PostMapping()
    public void create(@RequestBody WeaponType weaponTypes){
       service.saveType(weaponTypes);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable long id) {
		service.deleteType(id);
	}
	
	@PutMapping("{id}")
	public void update(@PathVariable long id, @RequestBody WeaponType weaponTypes){
       service.updateType(id, weaponTypes);
	}
	
}
