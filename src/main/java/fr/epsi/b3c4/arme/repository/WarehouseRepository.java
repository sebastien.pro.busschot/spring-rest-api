package fr.epsi.b3c4.arme.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.epsi.b3c4.arme.models.Warehouse;

public interface WarehouseRepository  extends JpaRepository<Warehouse, Long> {}
